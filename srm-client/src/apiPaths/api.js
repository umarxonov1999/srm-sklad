export const api = {
    //Auth
    login: 'auth/login',
    me: 'users/me',

    //Category
    allCategoriesByPageable: 'category/allByPageable',
    saveOrEditCategory: 'category/saveOrEdit',
    changeActiveOfCategory: 'category/changeActive/',
    remove: 'category/delete/',
}