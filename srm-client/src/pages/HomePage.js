import React, {Component} from 'react';
import {AvForm,AvField} from 'availity-reactstrap-validation'
import imgOsh from '../photos/osh.jpg'
import axios from "axios";
import {BASE_URL, TOKEN_NAME, TOKEN_TYPE} from "../utills/constants";
import {api} from '../apiPaths/api'
import jwtDecode from "jwt-decode";
import {connect} from "react-redux";
import {login} from "../redux/actions/AuthAction";
import {toast} from "react-toastify";
class HomePage extends Component {
    render() {
        const login=(e,v)=>{
            console.log(v,'VALUES')
            axios.post(BASE_URL+api.login,v).then(res=>{
                if (res.status===200){
                    toast.success("Success")
                    localStorage.setItem(TOKEN_NAME,TOKEN_TYPE+res.data)
                    let parsedToken=jwtDecode(res.data)
                    console.log(parsedToken.roles[0].roleName,'PARSED TOKEN')
                    this.props.signIn(parsedToken.roles[0].roleName)
                    if (parsedToken.roles[0].roleName==='ROLE_ADMIN'){
                        this.props.history.push("/dashboard")
                    }
                    else if (parsedToken.roles[0].roleName==='ROLE_MANAGER'){
                        this.props.history.push("/reports")
                    }
                    else if (parsedToken.roles[0].roleName==='ROLE_SELLER'){
                        this.props.history.push("/sale")
                    }else {
                        this.props.history.push("/")
                    }
                }

            }).catch(res=>{
                toast.error("Error")
            })
        }
        let applying = {
            backgroundImage: 'url(' + imgOsh + ')',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'right center, left top',
            backgroundSize: '100%, 100%',
            width: '100%',
            height: '100vh',
        }
        return (
            <div>
                <div className="row">
                    <div className="col-md-4 p-5">
                        <div className="row mt-5">
                            <div className="col-md-12">
                                <AvForm onValidSubmit={login}>
                                    <AvField type={'text'} label={'Enter username : '} name={'username'}/>
                                    <AvField type={'password'} label={'Enter password : '} name={'password'}/>
                                    <button type={'submit'} className={'btn btn-success mt-2'}>Login</button>
                                </AvForm>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-8">
                        <div style={applying}></div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatch={
    signIn:login
}

export default connect(null,mapDispatch)(HomePage);