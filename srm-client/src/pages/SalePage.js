import React, {Component} from 'react';
import NavBar from "../components/NavBar";

class SalePage extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-md-2">
                    <NavBar active={3}/>
                </div>
                <div className="col-md-10">
                    <h1>This is Sale Page</h1>
                </div>
            </div>
        );
    }
}

export default SalePage;