import {request} from "../utills/Request";
import {api} from "../apiPaths/api";

class AdminRequest{
    //Auth
    static async me(){
        let user=''
        await request('get',api.me).then(res=>{
        user= res.data
        }).catch(res=>{
            user=null
        })
        return user
    }

    //Category
    static async getAllCategoryByPageable(params){
        let categories=[]
        await request('get',api.allCategoriesByPageable+'?page='+params.page+'&size='+params.size+'&search='+params.search).then(res=>{
            categories= res.data
        }).catch(res=>{
            categories=[]
        })
        return categories
    }
    static async changeActiveOfCategory(id){
        let categories=[]
        await request('get',api.changeActiveOfCategory+id).then(res=>{
            categories= res.data
        }).catch(res=>{
            categories=[]
        })
        return categories
    }
    static async saveOrEditCategory(item){
        let response=''
        await request('post',api.saveOrEditCategory,item).then(res=>{
            response=res
        }).catch(res=>{
            response=''
        })
        return response
    }
    static async remove(id){
        let categories=[]
        await request('get',api.remove+id).then(res=>{
            categories= res.data
        }).catch(res=>{
            categories=[]
        })
        return categories
    }
}
export default AdminRequest