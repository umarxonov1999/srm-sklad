import {BrowserRouter, Switch, Route} from "react-router-dom";
import HomePage from "./pages/HomePage";
import Dashboard from "./pages/Dashboard";
import SalePage from "./pages/SalePage";
import {combineReducers, createStore} from 'redux'
import auth from "./redux/reducers/authReducer";
import {Provider} from "react-redux";
import Orders from "./pages/Orders";
import Reports from "./pages/Reports";
import Category from "./pages/Category";
import Supplier from "./pages/Supplier";
import Warehouse from "./pages/Warehouse";
import Products from "./pages/Products";
import KPI from "./pages/KPI";
import Employee from "./pages/Employee";
import Settings from "./pages/Settings";
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const allReducers = combineReducers(
    auth
)
const store = createStore(
    allReducers
)
const App = () => {
    return <BrowserRouter>
        <Provider store={store}>
            <ToastContainer/>
            <Switch>
                <Route path="/dashboard" component={Dashboard}/>
                <Route path="/reports" component={Reports}/>
                <Route path="/orders" component={Orders}/>
                <Route path="/sale" component={SalePage}/>
                <Route path="/category" component={Category}/>
                <Route path="/supplier" component={Supplier}/>
                <Route path="/warehouse" component={Warehouse}/>
                <Route path="/products" component={Products}/>
                <Route path="/kpi" component={KPI}/>
                <Route path="/employee" component={Employee}/>
                <Route path="/settings" component={Settings}/>
                <Route path="/" component={HomePage}/>
            </Switch>
        </Provider>
    </BrowserRouter>
}
export default App