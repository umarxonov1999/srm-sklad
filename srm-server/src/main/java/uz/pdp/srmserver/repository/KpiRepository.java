package uz.pdp.srmserver.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.srmserver.entitiy.Kpi;

public interface KpiRepository extends JpaRepository<Kpi, Integer> {
    Page<Kpi> findByActive(Boolean active, Pageable simplePageable);
}