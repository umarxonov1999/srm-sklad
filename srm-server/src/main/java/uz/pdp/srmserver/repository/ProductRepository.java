package uz.pdp.srmserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.srmserver.entitiy.Product;

import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {
}