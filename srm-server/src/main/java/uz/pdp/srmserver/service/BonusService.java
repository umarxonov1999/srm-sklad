package uz.pdp.srmserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.srmserver.entitiy.Bonus;
import uz.pdp.srmserver.payload.ApiResponse;
import uz.pdp.srmserver.payload.BonusDto;
import uz.pdp.srmserver.repository.BonusRepository;
import uz.pdp.srmserver.repository.UserRepository;
import uz.pdp.srmserver.utils.CommonUtills;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class BonusService {

    @Autowired
    BonusRepository bonusRepository;

    @Autowired
    UserRepository userRepository;

    public ApiResponse saveOrEdit(BonusDto dto){

        try {
            Bonus bonus=new Bonus();
            if (dto.getId()!=null){
               bonus=bonusRepository.getById(dto.getId());
            }

            bonus.setUser(userRepository.getById(dto.getUser()));
            bonus.setBonusSum(dto.getBonusSum());
            bonus.setDescription(bonus.getDescription());
            bonus.setApproved(dto.isApproved());

            bonusRepository.save(bonus);

            return new ApiResponse(dto.getId()!=null?"Edited":"Saved",true);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ApiResponse("Error",false);
    }

    public ApiResponse getAll(int page, int size, String startDate, String endDate, Boolean approved) throws IllegalAccessException {
        try {
            Page<Bonus> bonusPage = bonusRepository.findAllByApprovedAndCreatedAtBetween(approved,CommonUtills.getDateFromString(startDate),CommonUtills.getDateFromString(endDate),CommonUtills.getPageableByCreatedAtDesc(page, size));
            return new ApiResponse(true,"Ok",bonusPage.getContent().stream().map(this::getBonusDto).collect(Collectors.toList()),bonusPage.getTotalElements());
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ApiResponse("Error",false);
    }

    public Bonus getOne(UUID id){
        return bonusRepository.getById(id);
    }

    public ApiResponse delete(UUID id){
        try {
            bonusRepository.deleteById(id);
            return new ApiResponse("Deleted!",true);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ApiResponse("Error",false);
    }

    public BonusDto getBonusDto(Bonus bonus){
        BonusDto dto = new BonusDto();
        dto.setId(bonus.getId());
        dto.setUser(bonus.getUser().getId());
        dto.setBonusSum(bonus.getBonusSum());
        dto.setDescription(bonus.getDescription());
        dto.setApproved(bonus.isApproved());
        return dto;
    }



}
