package uz.pdp.srmserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.srmserver.entitiy.Bonus;
import uz.pdp.srmserver.entitiy.Kpi;
import uz.pdp.srmserver.payload.ApiResponse;
import uz.pdp.srmserver.payload.BonusDto;
import uz.pdp.srmserver.payload.KpiDto;
import uz.pdp.srmserver.repository.KpiRepository;
import uz.pdp.srmserver.repository.RoleRepository;
import uz.pdp.srmserver.repository.UserRepository;
import uz.pdp.srmserver.utils.CommonUtills;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class KpiService {

    @Autowired
    KpiRepository kpiRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    public ApiResponse save(KpiDto dto){

        try {
            Kpi kpi=new Kpi();
            if (dto.getId()!=null){
               kpi=kpiRepository.getById(kpi.getId());
            }
            kpi.setDescription(dto.getDescription());
            kpi.setName(dto.getName());
            kpi.setActive(dto.isActive());
            kpi.setRole(roleRepository.getById(dto.getRoleId()));
            kpi.setPercent(dto.getPercent());
            kpi.setMinSum(dto.getMinSum());
            kpi.setMaxSum(dto.getMaxSum());

            kpiRepository.save(kpi);

            return new ApiResponse("Saved",true);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ApiResponse("Error",false);
    }

    public ApiResponse getAll(Integer page, Integer size, Boolean active) throws IllegalAccessException {
        try {
            Page<Kpi> kpiPage = kpiRepository.findByActive(active, CommonUtills.simplePageable(page, size));
            return new ApiResponse(true, "Ok", kpiPage.getContent().stream().map(this::getKpiDto).collect(Collectors.toList()),kpiPage.getTotalElements());
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ApiResponse("error",false);
    }
    public Kpi getOne(Integer id){
        return kpiRepository.getById(id);
    }

    public ApiResponse delete(Integer id){

        try {
            Kpi kpi=kpiRepository.getById(id);


            kpiRepository.delete(kpi);
            return new ApiResponse("Deleted!",true);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ApiResponse("Error",false);
    }


    public KpiDto getKpiDto(Kpi kpi){
        KpiDto kpiDto=new KpiDto();
        kpiDto.setActive(kpi.isActive());
        kpiDto.setId(kpi.getId());
        kpiDto.setDescription(kpi.getDescription());
        kpiDto.setMaxSum(kpi.getMaxSum());
        kpiDto.setName(kpi.getName());
        kpiDto.setPercent(kpi.getPercent());
        kpiDto.setRole(kpi.getRole());
        return kpiDto;
    }
}
